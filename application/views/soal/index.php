<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg-8">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>
    <?php foreach ($ujian as $element) { ?>
        <div class="card" style="width: 18rem;display:block; margin: 10px;float:left;height: 20 rem;font-size: 12px">
            <img class="card-img-top" src="https://www.qureta.com/uploads/post/73921_96809.jpg" alt="Card image cap">
            <div class="card-body">
                <table>
                    <tr>
                        <td>Ujian</td>
                        <td>:</td>
                        <td>
                            <p class="card-text"><?= $element['nama'] ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>
                            <p class="card-text">
                                <?php
                                $date = date_create($element['tanggal_ujian']);
                                echo date_format($date, "Y-m-d");
                                ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>Jam</td>
                        <td>:</td>
                        <td>
                            <p class="card-text">
                                <?php
                                $date = date_create($element['tanggal_ujian']);
                                echo date_format($date, "H:i:s");
                                ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td><button class="btn btn-primary"
                        <?php 
                        date_default_timezone_set("Asia/Jakarta");
                        $buttonDate = date_format($date, "Y-m-d");
                        $buttonTime = date_format($date, "H:i:s");
                        $nowdate = date("Y-m-d");
                        $nowtime = date("H:i:s");
                        ?>
                        <?php if($nowdate != $buttonDate){ ?>
                        disabled
                        <?php } elseif($nowtime < $buttonTime ){?>
                        disabled
                        <?php }elseif($nowtime = date($buttonTime, time() + 60*$element['durasi'] )){ ?>
                        disabled
                        <?php } ?>
                        >kerjakan</button></td>
                    </tr>
                </table>
            </div>
        </div>
    <?php } ?>
</div>
</div>
<!-- /.container-fluid -->