  <!-- Sidebar -->
  <ul class="navbar-nav bg-white sidebar sidebar-light shadow accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
          <div class="sidebar-brand-icon rotate-n-15">
             <img src="<?= base_url()?>/assets/img/icon.png" alt="" height="35" width="35">
          </div>
          <div class="sidebar-brand-text mx-3">E-Arsip</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider ">

      <!-- query menu -->
      <?php
        $role_id = $this->session->userdata('role_id');
        $queryMenu = "
                    SELECT `user_menu`.`id`,`menu`
                FROM `user_menu` JOIN  `user_access_menu` 
                    ON `user_menu`.`id`=`user_access_menu`.`menu_id`
                    WHERE `user_access_menu`.`role_id`=$role_id
                    Order By `user_access_menu`.`menu_id` ASC
            ";
        $menu = $this->db->query($queryMenu)->result_array();
        ?>
      <!-- Heading -->
      <?php foreach ($menu as $m) : ?>
          <div class="sidebar-heading">
              <?= $m['menu'] ?>
          </div>

          <?php
                $menuId = $m['id'];
                $querySubMenu = "
                        select * from `user_sub_menu` join `user_menu` on `user_sub_menu`.`menu_id`= `user_menu`.`id`
                        where `user_sub_menu`.`menu_id`=$menuId
                        and `user_sub_menu`.`is_active`=1 ";
                $submenu = $this->db->query($querySubMenu)->result_array();
                ?>
          <?php foreach ($submenu as $sm) : ?>
              <?php if ($title == $sm['title']) : ?>
                  <li class="nav-item active">
                  <?php else : ?>
                  <li class="nav-item">
                  <?php endif; ?>
                  <a class="nav-link pb-0" href="<?= base_url($sm['url']); ?>">
                      <i class="<?= $sm['icon']; ?>"></i>
                      <span><?= $sm['title']; ?></span></a>
                  </li>
              <?php endforeach; ?>
              <hr class="sidebar-divider mt-3">
          <?php endforeach; ?>
          <!-- Nav Item - Charts -->
          <li class="nav-item">
              <a class="nav-link" href="<?= base_url('Auth/logout'); ?>">
                  <i class="fas fa-fw fa-sign-out-alt"></i>
                  <span>Logout</span></a>
          </li>
          <!-- Divider -->
          <hr class="sidebar-divider">
          <!-- Sidebar Toggler (Sidebar) -->
          <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
          </div>

  </ul>
  <!-- End of Sidebar -->