<div class="container-fluid">

    <!-- Page Heading -->
    <h2 class="h3 mb-4 text-gray-800"><?= $title; ?></h2>
    <div class="row">
        <div class="col-lg-12">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Akademik Folder</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Option</div>
                                <a class="dropdown-item" href="#">Add Folder</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-success">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C1</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-primary">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C2</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-warning">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C3</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-info">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C4</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-danger">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C5</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card mb-3 shadow" style="max-width: 18rem;border: 0">
                                        <div class="card-header text-white bg-dark">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2>C6</h2>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fas fa-folder-open" style="font-size:100px;"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-0 text-red">
                                            <h5 class="card-title">success card title</h5>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        </div>
                                        <div class="card-footer border-0">
                                            <div class="text-left">
                                                <badge class="badge badge-primary">edit</badge>
                                                <badge class="badge badge-info">open</badge>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>