<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-lg-5" style="margin-top: 50px">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <!-- <div class="col-lg-7 d-none d-lg-block" style="background-image: url(./assets/img/logo.jpeg);background-repeat: repeat-y;background-size: auto;"></div> -->
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <img src="./assets/img/icon2.png" alt="" height="150" width="150">
                                    <!-- <h1 class="h4 text-gray-900 mb-4">Login</h1> -->
                                </div>
                                <?= $this->session->flashdata('message'); ?>
                                <form class="user" method="POST" action="<?= base_url('auth'); ?>">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" name="email" value="<?= set_value('email') ?>" id="email" placeholder="Enter Email Address...">
                                        <?= form_error('email', ' <small class="text-danger">', '</small> ') ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                        <?= form_error('password', ' <small class="text-danger">', '</small> ') ?>
                                    </div>
                                    <button type="submit" class="btn btn-warning btn-user btn-block">
                                        Login
                                    </button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <p style="font-size:10px">Please login using email already registered by administrator</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>