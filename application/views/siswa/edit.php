<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg-8">
            <?= form_open_multipart('siswa/editsiswa'); ?>
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" value="<?= $siswa['id_siswa'] ?>" class="form-control" id="id_siswa" name="id_siswa">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $siswa['nisn'] ?>" class="form-control" id="nisn" name="nisn" placeholder="Nisn..">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $siswa['nama'] ?>" class="form-control" id="nama" name="nama" placeholder="Nama..">
                </div>
                <div class="form-group">
                    <input type="text" value="<?= $siswa['kelas'] ?>" class="form-control" id="kelas" name="kelas" placeholder="Kelas..">
                </div>
                <div class="form-group">
                    <select id="jurusan" class="form-control" name="jurusan" value="<?= $siswa['jurusan'] ?>">
                        <option value="" disabled selected>Pilih Jurusan</option>
                        <option <?php if ($siswa['jurusan'] == 'Jaringan') echo 'selected'; ?> value="Jaringan">Jaringan</option>
                        <option <?php if ($siswa['jurusan'] == 'Akuntansi') echo 'selected'; ?> value="Akuntansi">Akuntansi</option>
                        <option <?php if ($siswa['jurusan'] == 'Rekayasa Perangkat Lunak') echo 'selected'; ?> value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                    </select>
                </div>
                <div class="form-group">
                    <select name="status" id="status" class="form-control"> value="<?= $siswa['status'] ?>"
                        <option value="">Pilih Status</option>
                        <option <?php if ($siswa['status'] == '1') echo 'selected'; ?> value="1">Active</option>
                        <option <?php if ($siswa['status'] == '2') echo 'selected'; ?> value="2">Not Active</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>

        </div>
    </div>

</div>
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->