<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['siswa'] = $this->db->get('siswa')->result_array();

        $data['title'] = 'Siswa Management';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/index', $data);
        $this->load->view('templates/footer');
    }
    public function add()
    {
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['siswa'] = $this->db->get('siswa')->result_array();
        $this->form_validation->set_rules('nisn', 'Nisn', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('kelas', 'Kelas', 'required');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Siswa Management';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('siswa/index', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nisn' => $this->input->post('nisn'),
                'nama' => $this->input->post('nama'),
                'kelas' => $this->input->post('kelas'),
                'jurusan' => $this->input->post('jurusan'),
                'status' => $this->input->post('status')
            ];
            $this->db->insert('siswa', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            New siswa Added
            </div>
            ');
            redirect('siswa');
        }
    }
    public function editsiswa()
    {
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['siswa'] = $this->db->get('siswa')->result_array();
        $this->form_validation->set_rules('nisn', 'Nisn', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('kelas', 'Kelas', 'required');
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Siswa Management';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('siswa/index', $data);
            $this->load->view('templates/footer');
        } else {
            $id_siswa = $this->input->post('id_siswa');
            $data = [
                'nisn' => $this->input->post('nisn'),
                'nama' => $this->input->post('nama'),
                'kelas' => $this->input->post('kelas'),
                'jurusan' => $this->input->post('jurusan'),
                'status' => $this->input->post('status')
            ];
            $this->db->set($data);
            $this->db->where('id_siswa', $id_siswa);
            $this->db->update('siswa');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            New siswa Added
            </div>
            ');
            redirect('siswa');
        }
    }
    public function edit($siswa_id)
    {
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['siswa'] = $this->db->get_where('siswa', ['id_siswa' => $siswa_id])->row_array();
        $this->db->where('id_siswa !=', 1);
        $data['title'] = 'Edit Siswa';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('siswa/edit', $data);
        $this->load->view('templates/footer');
    }
    public function delete($siswa_id)
    {
        $this->db->where('id_siswa', $siswa_id);
        $this->db->delete('siswa');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        siswa has been deleted
        </div>
        ');
        redirect('siswa');
    }
}
